groups:
- name: GitLab Component Operations-per-Second Rates
  interval: 1m
  rules:
  # pages - haproxy service in front of GitLab pages
  - record: gitlab_component_ops:rate
    labels:
      component: 'haproxy_http'
    expr: >
      sum (rate(haproxy_backend_http_responses_total{type="pages"}[1m])) by (environment, type, tier, stage)

  - record: gitlab_component_ops:rate
    labels:
      component: 'haproxy_https'
    expr: >
      sum(haproxy_backend_current_session_rate{backend="pages_https", type="pages"}) by (environment, type, tier, stage)

  # Stackdriver
  - record: gitlab_component_ops:rate
    labels:
      type: stackdriver
      tier: inf
      stage: main
    expr: >
      sum(
        label_replace(
          stackdriver_gce_instance_logging_googleapis_com_log_entry_count, "component", "$1", "log", "(.*)"
        )
      ) by (environment, component)

######################
# Aggregation Stage
######################

- name: GitLab Service Operations-per-Second Aggregated Rates
  interval: 1m
  rules:
  # Aggregate over all components within a service
  - record: gitlab_service_ops:rate
    expr: >
      sum by (environment, tier, type, stage) (gitlab_component_ops:rate >= 0)

  # Aggregate over all components within a service, for a node
  - record: gitlab_service_node_ops:rate
    expr: >
      sum by (environment, tier, type, stage, shard, fqdn) (gitlab_component_node_ops:rate >= 0)

- name: GitLab Component Operations-per-Second Rate Stats
  interval: 5m
  rules:
  # Average values for each component, over a week
  - record: gitlab_component_ops:rate:avg_over_time_1w
    expr: >
      avg_over_time(gitlab_component_ops:rate[1w])
  # Stddev for each component, over a week
  - record: gitlab_component_ops:rate:stddev_over_time_1w
    expr: >
      stddev_over_time(gitlab_component_ops:rate[1w])

- name: GitLab Service Operations-per-Second Rate Stats
  interval: 5m
  rules:
  # Average values for each service, over a week
  - record: gitlab_service_ops:rate:avg_over_time_1w
    expr: >
      avg_over_time(gitlab_service_ops:rate[1w])
  # Stddev for each service, over a week
  - record: gitlab_service_ops:rate:stddev_over_time_1w
    expr: >
      stddev_over_time(gitlab_service_ops:rate[1w])

- name: GitLab Service Ops Rate Weekly Periodic Values
  interval: 5m
  rules:
  # Predict what the value should be using the median value for a
  # four hour period, for the past 3 weeks include week-on-week growth...
  - record: gitlab_service_ops:rate:prediction
    expr: >
      quantile(0.5,
        label_replace(
          avg_over_time(
            gitlab_service_ops:rate[4h] offset 166h # 1 week - 2 hours
          )
          + gitlab_service_ops:rate:avg_over_time_1w - gitlab_service_ops:rate:avg_over_time_1w offset 1w
          , "p", "1w", "", "")
        or
        label_replace(
          avg_over_time(
            gitlab_service_ops:rate[4h] offset 334h # 2 weeks - 2 hours
          )
          + gitlab_service_ops:rate:avg_over_time_1w - gitlab_service_ops:rate:avg_over_time_1w offset 2w
          , "p", "2w", "", "")
        or
        label_replace(
          avg_over_time(
            gitlab_service_ops:rate[4h] offset 502h # 3 weeks - 2 hours
          )
          + gitlab_service_ops:rate:avg_over_time_1w - gitlab_service_ops:rate:avg_over_time_1w offset 3w
          , "p", "3w", "", "")
      )
      without (p)
